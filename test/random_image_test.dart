

import 'package:fidibo_test_project/services/random_image.dart';
import 'package:flutter_test/flutter_test.dart';

main(){

  group('Random Image', (){

    test('returns an first string image link from List of images in default state', () async{

      final randomImage = RandomImage();
      expect(randomImage.defaultImage(), randomImage.images[0]);

    });

    test('returns an Random string image link', () async{

      final randomImage = RandomImage();
      expect(randomImage.findRandomImage(), isA<String>());

    });

  });


}