import 'package:fidibo_test_project/blocs/translate_widget_cubit/transform_widget_cubit.dart';
import 'package:fidibo_test_project/blocs/translate_widget_cubit/transform_widget_state.dart';
import 'package:fidibo_test_project/constas.dart';
import 'package:fidibo_test_project/widgets/image_square.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TransformSquarePage extends StatefulWidget{
  @override
  TransformSquarePageState createState() => TransformSquarePageState();
}

class TransformSquarePageState extends State<TransformSquarePage> with TickerProviderStateMixin {

  late AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
    );

    controller.addListener(() {
      //update position
      BlocProvider.of<TransformImageCubit>(context).updateWidgetPosition();

    });

    controller.repeat();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    BlocProvider.of<TransformImageCubit>(context).screenSize = MediaQuery.of(context).size;

    return  Scaffold(
            backgroundColor: Color(0xffFFFFFF),
            appBar: AppBar(
              backgroundColor: Colors.black,
              title: Text("Fidibo Task"),
              centerTitle: true,
            ),
            body: GestureDetector(
              onHorizontalDragUpdate:(DragUpdateDetails event ){
                BlocProvider.of<TransformImageCubit>(context).onSwipe(event.localPosition);
              },
              onVerticalDragUpdate:(DragUpdateDetails event ){
                BlocProvider.of<TransformImageCubit>(context).onSwipe(event.localPosition);
              },
              child: Stack(
                children: [
                  BlocBuilder<TransformImageCubit, TransformWidgetState>(
                      builder: (context, state) {
                        if (state is TransformNewPositionState)
                          return Positioned(
                            //"- IMAGE_SIZE / 2" this is for center of widget
                            top: state.offset.dy - IMAGE_SIZE / 2,
                            left: state.offset.dx  - IMAGE_SIZE / 2,
                            child: ImageSquare.show(context),
                          );
                        //AnimatedPositioned for initial state in middle of page
                        return Positioned(
                          top: (size.height / 2) - IMAGE_SIZE,
                          left: (size.width - IMAGE_SIZE) / 2,
                          child:  ImageSquare.show(context),

                        );
                      }),
                ],
              ),
            ));
  }
}
