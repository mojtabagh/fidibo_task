import 'package:flutter/cupertino.dart';

@immutable
class ImageCubitState {
  @override
  List<Object> get props => [];
}

@immutable
class FindImageState extends ImageCubitState{
  final String image;

  FindImageState(this.image);
  @override
  List<Object> get props => [image];

}