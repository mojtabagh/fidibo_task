
import 'package:bloc/bloc.dart';
import 'package:fidibo_test_project/services/random_image.dart';
import 'image_cubit_state.dart';


class ImageCubit extends Cubit<ImageCubitState> {

  RandomImage randomImage;
  ImageCubit(this.randomImage) : super(FindImageState(randomImage.defaultImage()));

  findNewRandomImage() => emit(FindImageState(randomImage.findRandomImage()));

}
