import 'package:bloc/bloc.dart';
import 'package:fidibo_test_project/blocs/translate_widget_cubit/transform_widget_state.dart';
import 'package:fidibo_test_project/utilitis/throttle.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../constas.dart';

class TransformImageCubit extends Cubit<TransformWidgetState> {
  Offset widgetDirection = Offset(1, 1);
  double widgetXSpeed = 0;
  double widgetYSpeed = 0;
  Size _screenSize = Size(200, 200);

  //this throttle is for get last offset in swipe
  final swipeThrottle = Throttling(duration: const Duration(milliseconds: 50));

  TransformImageCubit() : super(TransformInitState());

  set screenSize(Size value) {
    _screenSize = value;
    widgetDirection = Offset(_screenSize.width / 2, _screenSize.height / 2);
  }

  onSwipe(Offset offset) {
    swipeThrottle.throttle(() {
      calculateNewDirection(offset);
    });
  }

  calculateNewDirection(Offset newOffset){
    //this is for find direction
    Offset directionOfNewOffset = Offset(
        newOffset.dx - widgetDirection.dx, newOffset.dy - widgetDirection.dy);

    if (directionOfNewOffset.dx.abs() > DIRECT_SWIPE_OFFSET) {
      if (directionOfNewOffset.dx < 0)
        widgetXSpeed = -WIDGET_TRANSFORM_SPEED;
      else
        widgetXSpeed = WIDGET_TRANSFORM_SPEED;
    } else
      //move on direct line
      widgetXSpeed = 0;

    if (directionOfNewOffset.dy.abs() > DIRECT_SWIPE_OFFSET) {
      if (directionOfNewOffset.dy < 0)
        widgetYSpeed = -WIDGET_TRANSFORM_SPEED;
      else
        widgetYSpeed = WIDGET_TRANSFORM_SPEED;
    } else
      //move on direct line
      widgetYSpeed = 0;
  }

  updateWidgetPosition() {
    if (widgetDirection.dx >= _screenSize.width - IMAGE_SIZE/2 ||
        widgetDirection.dx <= IMAGE_SIZE/2) {
      //re-turn
      widgetXSpeed = widgetXSpeed * -1;
    }

    if (widgetDirection.dy >= _screenSize.height - IMAGE_SIZE ||
        widgetDirection.dy <= IMAGE_SIZE/2) {
      //re-turn
      widgetYSpeed = widgetYSpeed * -1;
    }

    widgetDirection = Offset(
        widgetDirection.dx + widgetXSpeed, widgetDirection.dy + widgetYSpeed);

    emit(TransformNewPositionState(widgetDirection));
  }
}
