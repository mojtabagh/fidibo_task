import 'package:flutter/cupertino.dart';

@immutable
class TransformWidgetState {
  @override
  List<Object> get props => [];
}

class TransformInitState extends TransformWidgetState{}

@immutable
class TransformNewPositionState extends TransformWidgetState{
  final Offset offset;

  TransformNewPositionState(this.offset);

  @override
  List<Object> get props => [offset];
}