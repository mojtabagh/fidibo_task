import 'package:fidibo_test_project/blocs/image_cubit/image_cubit.dart';
import 'package:fidibo_test_project/blocs/image_cubit/image_cubit_state.dart';
import 'package:fidibo_test_project/constas.dart';
import 'package:fidibo_test_project/services/random_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ImageSquare extends StatelessWidget {
  static Widget show<T>(BuildContext context) => BlocProvider<ImageCubit>(
        create: (context) => ImageCubit(RandomImage()),
        child: ImageSquare(),
      );

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ImageCubit, ImageCubitState>(builder: (context, state) {
      if (state is FindImageState) {
        // print(state.image);
        return Container(
            width: IMAGE_SIZE,
            height: IMAGE_SIZE,
            color: Colors.grey,
            child: InkWell(
              onTap: () {
                BlocProvider.of<ImageCubit>(context).findNewRandomImage();
              },
              child: Image.network(state.image,fit: BoxFit.fill, loadingBuilder:
                  (BuildContext context, Widget child,
                      ImageChunkEvent? loadingProgress) {
                if (loadingProgress == null) {
                  return child;
                }
                return Center(
                  child: CircularProgressIndicator(
                    value: loadingProgress.expectedTotalBytes != null
                        ? loadingProgress.cumulativeBytesLoaded /
                            loadingProgress.expectedTotalBytes!
                        : null,
                  ),
                );
              }),
            )
      );
      }
      return Container(
        width: IMAGE_SIZE,
        height: IMAGE_SIZE,
        color: Colors.grey,
      );
    });
  }
}
