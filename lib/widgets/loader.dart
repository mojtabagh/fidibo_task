import 'package:flutter/material.dart';

class Loader extends StatelessWidget {
  final double size;

  final Color color;

  Loader({this.size = 0, this.color = Colors.red});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: (size != 0)
            ? SizedBox(
          height: size,
          width: size,
          child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(color)),
        )
            : CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(color)),
      ),
    );
  }
}