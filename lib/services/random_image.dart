import 'dart:math';

class RandomImage{

  RandomImage._privateConstructor();

  static final RandomImage _instance = RandomImage._privateConstructor();

  factory RandomImage() {
    return _instance;
  }

  List<String> images = [
    'https://taghvimpars.com/assets/daily-images/5/19.jpg',
    'https://taghvimpars.com/assets/daily-images/5/20.jpg',
    'https://taghvimpars.com/assets/daily-images/5/21.jpg',
    'https://taghvimpars.com/assets/daily-images/5/22.jpg',
    'https://taghvimpars.com/assets/daily-images/5/23.jpg',
    'https://taghvimpars.com/assets/daily-images/5/24.jpg',
    'https://taghvimpars.com/assets/daily-images/5/25.jpg',
    'https://taghvimpars.com/assets/daily-images/5/26.jpg',
    'https://taghvimpars.com/assets/daily-images/5/27.jpg',
    'https://taghvimpars.com/assets/daily-images/5/28.jpg',
    'https://taghvimpars.com/assets/daily-images/5/29.jpg',
    'https://taghvimpars.com/assets/daily-images/5/30.jpg',
  ];

  String defaultImage() => images[0];

  String findRandomImage(){
    return images[Random().nextInt(12)];
  }


}