import 'package:fidibo_test_project/routes.dart';
import 'package:fidibo_test_project/screens/transform_square_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'blocs/translate_widget_cubit/transform_widget_cubit.dart';

void main() {
  runApp(MyApp());
}
final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Fidibo Task',
        navigatorKey: navigatorKey,
        debugShowCheckedModeBanner: false,
        routes: {
          Routes.home: (_) {
            return BlocProvider(
              create: (BuildContext context) =>
              TransformImageCubit(),
              child: TransformSquarePage(),
            );
          },
        });
  }
}
